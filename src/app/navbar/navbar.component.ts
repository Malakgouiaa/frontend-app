import { AuthService } from './../services/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  currentUser: string;

  constructor(
    private router: Router,
    private authenticationService: AuthService
  ) {
    this.currentUser = authenticationService.token;
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
    this.refresh();
  }

  refresh(): void {
    window.location.reload();
  }

}
