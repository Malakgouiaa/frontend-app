import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ContentService {

  constructor(
    private httpClient: HttpClient
  ) { }

  url = 'http://144.91.92.58/';

  findByEmail(email: string): Observable<HttpResponse<any>> {
    return this.httpClient.get(`${this.url}/findbyemail?email=${email}`, { observe: 'response' });
  }

}
