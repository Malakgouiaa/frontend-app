import { IResult } from './../model/result.model';
import { Pipe } from '@angular/core';

// tslint:disable-next-line: use-pipe-transform-interface
@Pipe({
  name: 'resultFilter',
})
export class ResultatPipe {

  transform(results: IResult[], searchText: string): IResult[] {
    if (!results || !searchText) {
      return results;
    }

    let searchTextToLowerCase: string = searchText.toLowerCase();
    return results.filter(result => {
      let res = result.username!.toLowerCase().includes(searchTextToLowerCase);
      return res;
    });
  }
}
