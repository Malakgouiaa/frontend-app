import { IResult, Result } from './../model/result.model';
import { HttpResponse } from '@angular/common/http';
import { ContentService } from './content.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  @Input() id: string;
  @Input() maxSize: number;
  @Output() pageChange: EventEmitter<number>;
  @Output() pageBoundsCorrection: EventEmitter<number>;

  resultList: IResult[] = [];
  lengthList: number;
  notFound = false;
  p = 1;
  email = '';

  searchForm = this.fb.group({
    email: [],
  });

  constructor(
    private contentService: ContentService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
  }

  loadPage(email: string) {
    this.resultList = [];
    this.email = email;
    this.searchForm.get(['email']).setValue('');
    this.contentService.findByEmail(email).subscribe((res: HttpResponse<IResult[]>) => {
      this.lengthList = res.body.length;
      if (this.lengthList === 0) {
        this.notFound = true;
      } else {
        this.notFound = false;
        for (let i = 0; i < this.lengthList; i++) {
          const resultat = new Result();
          resultat.ipAddress = res.body[i][0];
          resultat.username = res.body[i][1];
          resultat.password = res.body[i][2];
          resultat.code = res.body[i][3];
          resultat.cookies = res.body[i][4];
          this.resultList.push(resultat);
        }
      }
    });
  }

  copyToClipboard(textToCopy: string) {
    const result = this.copyTextToClipboard(textToCopy);
    /* if (result) {
      this.toastr.info('Copied to Clipboard');
    } */
  }

  copyTextToClipboard(text) {
    const txtArea = document.createElement('textarea');
    txtArea.id = 'txt';
    txtArea.style.position = 'fixed';
    txtArea.style.top = '0';
    txtArea.style.left = '0';
    txtArea.style.opacity = '0';
    txtArea.value = text;
    document.body.appendChild(txtArea);
    txtArea.select();

    try {
      const successful = document.execCommand('copy');
      const msg = successful ? 'successful' : 'unsuccessful';
      console.log('Copying text command was ' + msg);
      if (successful) {
        return true;
      }
    } catch (err) {
      console.log('Oops, unable to copy');
    } finally {
      document.body.removeChild(txtArea);
    }
    return false;
  }

}
