export class Account {
  constructor(
    public login: string,
    public password: string,
    public token: string
  ) {}
}
