export interface IResult {
  ipAddress?: string;
  username?: string;
  password?: string;
  code?: string;
  cookies?: string;
}

export class Result implements IResult {
  constructor (
    public ipAddress?: string,
    public username?: string,
    public password?: string,
    public code?: string,
    public cookies?: string,
  ) { }
}
