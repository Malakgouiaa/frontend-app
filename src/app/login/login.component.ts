import { Router, ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { AuthService } from './../services/auth.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {

  tokenKey: string;
  errorMsg: string;
  isLoggedIn = false;
  isLoginFailed = false;
  returnUrl: string;

  loginForm = this.fb.group({
    username: [],
    password: []
  });

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) {
    if (this.authService.token) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  login() {
    const username = this.loginForm.get(['username']).value;
    const password = this.loginForm.get(['password']).value;
    this.authService
      .login(username, password)
      .subscribe(
        (res: HttpResponse<any>) => {
          this.tokenKey = res.body.access_token;
          this.errorMsg = res.body.error;
          console.log('----------------------> ', res);
          if (this.tokenKey.length > 0) {
            localStorage.setItem('token', this.tokenKey);
            this.isLoginFailed = false;
            this.isLoggedIn = true;
            this.router.navigate([this.returnUrl]);
            this.refresh();
          }
          if (this.errorMsg.length > 0) {
            this.isLoginFailed = true;
          }
        }
      );
  }

  refresh(): void {
    window.location.reload();
  }
}
