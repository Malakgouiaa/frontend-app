import { Observable } from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';


export class re {
  constructor(
    public accessToken: string,
    public error: string,
  ) { }
}

@Injectable({ providedIn: 'root' })
export class AuthService {

  public token: string;

  // url here, helper/jwt.interceptor.ts , content/content.service.ts
  url = 'http://144.91.92.58/simplelogin';

  constructor(private http: HttpClient) {
    this.token = localStorage.getItem('token');
  }

  login(username: string, password: string): Observable<HttpResponse<any>> {
    const body = JSON.stringify(re);
    return this.http.post<any>(`${this.url}?username=${username}&password=${password}`, body, { observe: 'response' });
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('token');
  }

}
